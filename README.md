# pfsense captive portal para visitantes

Base para o captive portal utilizado no IFCE - Campus Quixadá. Esse captive portal é [baseado no
projeto disponível aqui](https://github.com/felixhaeberle/pfsense-captive-portal).

Foi adicionado um termo de uso, tendo como referência o disponibilizado pelo STI da UNIFESP.
[Acessível aqui](https://sti.unifesp.br/documentos/termos-de-uso-da-internet).

### Sistema para auto cadastro

O captive portal contém um link para o sistema de auto cadastro do Campus.
O sistema foi permite que o próprio usuário crie uma conta para acesso à Internet,
ele pode ser [acessado aqui](https://gitlab.com/ifce-quixada/wifi-visitantes).

## alguns links importantes

- [Official Pfsense Captive Portal Documentation](https://doc.pfsense.org/index.php/Captive_Portal)
- [Official Pfsense Captive Portal Voucher Documentation](https://doc.pfsense.org/index.php/Captive_Portal_Vouchers)
- [German HowTo Pfsense Captive Portal](http://www.nwlab.net/tutorials/pfSense/Captive-Portal.html)
